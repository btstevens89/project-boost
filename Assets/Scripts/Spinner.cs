using System.Threading;
using System;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [SerializeField] [Range(0,2f)] float rotationPerSecondX;
    [SerializeField] [Range(0,2f)] float rotationPerSecondY;
    [SerializeField] [Range(0,2f)] float rotationPerSecondZ;
    Vector3 rotationVector;
    float rotationFactor;
    //[SerializeField] float rotationPeriod = 2f; //uncomment if we call SinRotation below

    void Start()
    {
        rotationVector = new Vector3(rotationPerSecondX, rotationPerSecondY, rotationPerSecondZ);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationVector * 360 * Time.deltaTime); // this rotates it on a constant speed
    }
    void SinRotation()
    {
        //if (rotationPeriod <= Mathf.Epsilon) {return;} // Mathf.Epsilon is better to use than 0
        //float cycles = Time.time / rotationPeriod; // continually growing over time
        //const float tau = Mathf.PI * 2; // constant value of 6.283
        //float rawSinWave = MathF.Sin(cycles * tau); // going from -1 to 1
        //rotationFactor = (rawSinWave + 2f)/1; // recalculated to go from 0 to 1 so it's cleaner
        //Vector3 offset = rotationVector * rotationFactor;
        //transform.Rotate(offset); //this will rotate it on a sin wave
    }
}

