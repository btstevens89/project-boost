using System;
using UnityEngine;

public class Scaler : MonoBehaviour
{
    Vector3 startingScale;
    [SerializeField] Vector3 scaleDimensions;
    float movementFactor;
    [SerializeField] float scalePeriod = 2f;

    void Start()
    {
        startingScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (scalePeriod <= Mathf.Epsilon) {return;} // Mathf.Epsilon is better to use than 0
        float cycles = Time.time / scalePeriod; // continually growing over time
        const float tau = Mathf.PI * 2; // constant value of 6.283
        float rawSinWave = MathF.Sin(cycles * tau); // going from -1 to 1
        movementFactor = (rawSinWave + 1f)/2; // recalculated to go from 0 to 1 so it's cleaner
        Vector3 offset = scaleDimensions * movementFactor;
        transform.localScale = startingScale + offset;
    }
}
