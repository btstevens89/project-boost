using System;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    Vector3 startingPosition;
    [SerializeField] Vector3 movementVector;
    float movementFactor;
    [SerializeField] float oscillatorPeriod = 2f;

    void Start()
    {
        startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (oscillatorPeriod <= Mathf.Epsilon) {return;} // Mathf.Epsilon is better to use than 0
        float cycles = Time.time / oscillatorPeriod; // continually growing over time
        const float tau = Mathf.PI * 2; // constant value of 6.283
        float rawSinWave = MathF.Sin(cycles * tau); // going from -1 to 1
        movementFactor = (rawSinWave + 1f)/2; // recalculated to go from 0 to 1 so it's cleaner
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
    }
}
